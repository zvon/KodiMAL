# KodiMAL
A fork of XBMAL - https://github.com/kuruoujou/XBMal

Kodi MyAnimeList Updater
========================

* Author:	Zvon
* Date:		January 6, 2018
* Last Update:	April 14, 2018
* Version:	0.5.4
* Gitlab:	<https://gitlab.com/zvon/KodiMAL>
* Original Author: Spencer Julian (<helloThere@spencerjulian.com>)
* Original Github: <https://github.com/kuruoujou/XBMal>

This is free software released under version 3 of the GPL License.

This is KodiMAL, a fork of XBMAL, it's used to update your MAL Anime list using
Kodi's library information.

This is my first attempt at an add-on and I've not used python in a while
so there's probably a lot of room for improvement, if you notice such room, don't be
shy and create a pull request or something! You can fork the project and add
your improvements for all I care, we live in a free (software) world!

Usage
-----
You'll first want to backup your MAL account. I'm serious, I can't be sure this won't
break things, although it works fine for my list.

The easiest way to install is probably to download a release zip and install it
via Kodi's add-on installer (Add-ons->Install from zip file). But you can probably
just copy the service folder into your add-on directory.

After you install, you should configure it to use your username and password
(after you've backed up your MAL account of course!).

So now that configuration is complete, start the add-on! And get ready to wait!
The add-on will now go through all your TV Shows and try to find them on MAL.
This will take up to a few minutes, depending on how many TV Shows you've got.

After add-on fineshes you should see a list like this:

	Kodi show S1 -> MAL Show
	Kodi show S2 -> MAL Show
	Kodi show S1 -> Could not find mapping on MAL.
	Save above selections

Go through this list and edit all wrong guesses with a manual search. If you
want to remove a show entirely, choose `Hide this item` and it'll disappear.
If you accidentaly remove something, the only way to bring it back is to edit the
config file and change `malTitle` of the show from `Hide this item` to literally
anything else (just changing it to MAL title won't work, you need to select it in the
addon to get appropriate info for the item like the show ID and number of rewatches)

Once you have everything mapped, simply select the last item in the full list, labeled
`Save above selections`. This will write your mappings to it's configuration file.
And update your Anime List.

Now you can also add movies, this is done on per-movie basis, just find a movie
you'd like to add to MAL, bring up context menu (right click or hold OK button)
and select `Add Movie to MAL`. This will bring up the same window you saw when
you started the add-on, you can change the movie by clicking on it, just like
tv shows. Don't forget to save configuration.

Now that the config is written, it'll update your Anime List whenever you finish
watching an episode or a movie.

If you add a show to Kodi, you will need to re-run the setup script. However, the setup
script will read your previous mappings, so you should only need to update the newest items,
which are typically at the bottom of the list. It will also run much faster, as it won't
look up all of your shows again. It will only look up the ones it has no clue about.

Questions, Comments, Concerns, Issues
-------------------------------------

If you have any of these, you can [submit an issue](https://gitlab.com/zvon/KodiMAL/issues)

TODO
----
* When item is removed from Kodi library, remove from XML file (perhaps try to check with library cleaned?)
* Maybe, if we're really really lucky, and work on this really hard, get in the official repository?
